INSERT INTO profession (name) VALUES ('actor'),('compositor'),('director');
INSERT INTO genre (name) VALUES ('comedy'),('horror'),('drama');
INSERT INTO zodiak (name) VALUES ('gemini'), ('aries'),('leo');
INSERT INTO studio (name, creation_date, creator) VALUES ('	20th Century Fox','1935','Will Fox');
INSERT INTO movie_worker (movie, worker) VALUES ('Edward Scissorhands', 'Johnny Depp');
INSERT INTO award (name, creation_date, status) VALUES ('Oscar', '1929', 'Good');
INSERT INTO movie (name, genre, realese_date, budget, box_office, duration, rating) VALUES ('Edward Scissorhands', 'drama', '1990', '20mln', '86mln', '1.5h', '8.0');
INSERT INTO worker(name, birth_date, place_of_birth, height, state, zodiak, profession) VALUES ('Johnny Depp', '1963', 'USA','178cm', 'Kentucky', 'gemini', 'actor');