﻿create table profession(
	id_profession	serial primary key,
	name	varchar(50) not null
);

create table zodiak(
	id_zodiak	serial primary key,
	name	varchar(50) not null
);

create table worker(
	id_worker	serial primary key,
	name	varchar(50) not null,
	birth_date	date,
	place_of_birth	varchar(50),
	height	integer,
	state	integer,
	id_zodiak	integer not null,
	id_profession	integer not null,
	foreign key(id_zodiak) references zodiak(id_zodiak) on delete cascade,
	foreign key(id_profession) references profession(id_profession) on delete cascade
);

create table genre(
	id_genre serial primary key,
	name varchar(50) not null
);

create table movie(
	id_movie	serial primary key,
	name	varchar(50) not null,
	id_genre integer not null,
	release_date	date,
	budget	int check(budget > 10000),
	box_office	int,
	duration	int not null,
	rating	int check(rating between 0 and 10),
	foreign key(id_genre) references genre(id_genre) on delete cascade
);

create table movie_worker(
	foreign key(id_movie) references movie(id_movie) on delete cascade,
	foreign key(id_worker) references worker(id_worker) on delete cascade,
	id_movie_worker serial primary key,
	id_movie integer,
	id_worker integer
);

create table studio(
	id_studio	serial primary key,
	name	varchar(50) not null,
	creation_date	date,
	creator	varchar(50)
);

create table award(
	id_award	serial primary key,
	name	varchar(50) not null,
	creation_date	date,
	status	int
);

create table award_receiver(
	foreign key(id_award) references award(id_award) on delete cascade,
	foreign key(id_movie) references movie(id_movie) on delete cascade,
	foreign key(id_studio) references studio(id_studio) on delete cascade,
	foreign key(id_worker) references worker(id_worker) on delete cascade,
	id_award integer,
	id_movie integer,
	id_studio integer,
	id_worker integer
);